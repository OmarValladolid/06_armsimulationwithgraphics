#pragma once

#include "BodySection.h"
#include "glm/glm.hpp"

class ForearmSection : public BodySection
{
private:
	glm::vec3 _elbowPos;
	glm::vec3 _wristPos;

public:
	ForearmSection(glm::vec3 elbowPos, glm::vec3 wristPos, double mass, double length);
	~ForearmSection(void);

	glm::vec3 getElbowPosition(void);
	glm::vec3 getWristPosition(void);

	void setElbowPosition(glm::vec3 elbowPos);
	void setWristPosition(glm::vec3 wristPos);
};

