#include "ForearmSection.h"

ForearmSection::ForearmSection(glm::vec3 elbowPos, glm::vec3 wristPos, double mass, double length):BodySection(mass, length)
{
	this->_elbowPos = elbowPos;
	this->_wristPos = wristPos;
}

ForearmSection::~ForearmSection(void)
{}

glm::vec3 ForearmSection::getElbowPosition(void)
{
	return this->_elbowPos;
}

glm::vec3 ForearmSection::getWristPosition(void)
{
	return this->_wristPos;
}

void ForearmSection::setElbowPosition(glm::vec3 elbowPos)
{
	this->_elbowPos = elbowPos;
}

void ForearmSection::setWristPosition(glm::vec3 wristPos)
{
	this->_wristPos = wristPos;
}