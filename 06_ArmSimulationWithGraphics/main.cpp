// Glad and GLFW Libraries
#include <glad/glad.h>

// GLM Libraries
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"


// ImGui libraries
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

// C++ Libraries
#include <iostream>
#include <string>
//#include <vector>

#include <GLFW/glfw3.h>

// Own libraries
#include "Shader.h"
#include "Quadrilateral.h"
#include "camera.h"
#include "Arm.h"

using namespace std;

// Screen Settings
const unsigned int SCR_WIDTH = 800; //1280
const unsigned int SCR_HEIGHT = 800; //800


// Objects
Shader* shapeShader;
Shader* forearmShader;
Quadrilateral* upperArmShape;
Quadrilateral* forearmShape;
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

// Body section
Arm* arm;

// Control variables
static int sliderTheta = 0;
static int slider_kp = 0, slider_ki = 0, slider_kd = 0;

// Functions 
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
static void glfw_error_callback(int error, const char* description);
void processInput(GLFWwindow* window);



bool initGLFW(void);
bool initWindow(GLFWwindow* window);
bool initGLAD(void);
void init(void);
void display(GLFWwindow* window);

// ImGui functions
static void HelpMarker(const char* desc);

int main(int, char**)
{
    // Initialize GLFW
    if (!initGLFW())
        return -1;

	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Arm Simulation", NULL, NULL);

    // initialize Window
	if (!initWindow(window))
		return -1;

    // Initialize OpenGL loader
	if (!initGLAD())
		return -1;

    // Object initialization
    init();

    upperArmShape->buildGeometry(shapeShader);
    forearmShape->buildGeometry(shapeShader);

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;

    // Setup ImGui style
    ImGui::StyleColorsDark();

    // Setup PLatform/Rederer bindings
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");

    // State
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.0f);

    bool booleanValue = false;

    //glm::mat4 projection = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 10.0f);
    /*aspectRatio = ((GLdouble)width / (GLdouble)height);
    if (width <= height)
        gluOrtho2D(cs_left, cs_right, cs_bottom * aspectRatio, cs_top * aspectRatio);
    else
        gluOrtho2D(cs_left * aspectRatio, cs_right * aspectRatio, cs_bottom, cs_top);*/

    
    
    glm::mat4 view = camera.GetViewMatrix();

    shapeShader->use(); 
    shapeShader->setMat4("view", view);

    forearmShader->use();
    forearmShader->setMat4("view", view);

    while (!glfwWindowShouldClose(window))
    {

        float aspectRatio = (float)SCR_WIDTH / (float)SCR_HEIGHT;
        glm::mat4 projection;
        if (SCR_WIDTH <= SCR_HEIGHT)
            projection = glm::ortho(-10.0f, 50.0f, -50.0f * aspectRatio, 50.0f * aspectRatio, 0.0f, 10.0f);
        else
            projection = glm::ortho(-10.0f * aspectRatio, 50.0f * aspectRatio, -50.0f, 50.0f, 0.0f, 10.0f);
        /*if (SCR_WIDTH > SCR_HEIGHT)
            projection = glm::ortho(-10.0f, 50.0f, -50.0f * aspectRatio, 50.0f * aspectRatio, 0.0f, 10.0f);
        else
            projection = glm::ortho(-10.0f * aspectRatio, 50.0f * aspectRatio, -50.0f, 50.0f, 0.0f, 10.0f);*/

        shapeShader->use();
        shapeShader->setMat4("projection", projection);
        forearmShader->use();
        forearmShader->setMat4("projection", projection);

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        //Input
        processInput(window);
        //glClearColor(0.55f, 0.67f, 1.75f, 1.0f);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        display(window);

        {
            ImGui::Begin("Initial values");
            ImGui::SetWindowPos(ImVec2(20, 20), ImGuiCond_Once);
            ImGui::SetWindowSize(ImVec2(240, 300), ImGuiCond_Once);

            if(ImGui::TreeNode("Arm"))
            {
                if (ImGui::TreeNode("Upper arm"))
                {
                    ImGui::BulletText("Mass: %.3f kg", arm->upperArm->getMass());
                    ImGui::BulletText("Length: %.3f m", arm->upperArm->getLength());
                    ImGui::TreePop();
                }
                
                if (ImGui::TreeNode("Forearm"))
                {
                    ImGui::BulletText("Mass: %.3f kg", arm->forearm->getMass());
                    ImGui::BulletText("Length: %.3f m", arm->forearm->getLength());
                    ImGui::TreePop();
                }

                ImGui::TreePop();
            }

            ImGui::SliderInt("Theta", &sliderTheta, -90, 90);
            ImGui::SameLine(); HelpMarker("Angle between the upper arm and the forearm.");
            //ImGui::Text("Text inside the window");
            //ImGui::Checkbox("Hi", &booleanValue);
            //ImGui::Checkbox("Hello", &booleanValue);
            //ImGui::Button("Button");
            ImGui::End();
        }

        {
            ImGui::Begin("Play Controls", NULL, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize );
            ImGui::SetWindowPos(ImVec2(300, 20), ImGuiCond_Once);
            ImGui::SetWindowSize(ImVec2(155, 35), ImGuiCond_Once);
            ImGui::Button("Play");
            ImGui::SameLine();
            ImGui::Button("Pause");
            ImGui::SameLine();
            ImGui::Button("Stop");
            ImGui::End();
        }

        {
            ImGui::Begin("PID Controller");
            ImGui::SetWindowPos(ImVec2(500, 20), ImGuiCond_Once);
            ImGui::SetWindowSize(ImVec2(155, 150), ImGuiCond_Once);
            ImGui::SliderInt("Kp", &slider_kp, 0, 100);
            ImGui::SliderInt("Ki", &slider_ki, 0, 100);
            ImGui::SliderInt("Kd", &slider_kd, 0, 100);
            ImGui::Text("PID Output: 0.0");
            ImGui::Text("PID Error: 0.0");
            ImGui::End();
        }

        // Render
        ImGui::Render();


        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    upperArmShape->~Quadrilateral();
    forearmShape->~Quadrilateral();

    glfwDestroyWindow(window);
    glfwTerminate();

	return 0;
}

/// <summary>
/// Process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
/// </summary>
/// <param name="window">GLFW Window object</param>
void processInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        //stop = true;
        glfwSetWindowShouldClose(window, true);
    }
}

static void glfw_error_callback(int error, const char* description)
{
    cout << "GLFW Error " << error << ": " << description << endl;
}

/// <summary>
/// Whenever the window size changed (by OS or user resize) this callback function executes
/// </summary>
/// <param name="window">GLFW Window object</param>
/// <param name="width">Window width</param>
/// <param name="height">Window height</param>
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);

}

bool initGLFW(void)
{
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return false;
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    return true;
}

/// <summary>
/// GLFW window creation
/// </summary>
/// <returns>Window creation result</returns>
bool initWindow(GLFWwindow* window)
{
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return false;
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable Sync
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    return true;
}

/// <summary>
/// Load all OpenGL function pointers
/// </summary>
/// <returns>GLAD initialization result</returns>
bool initGLAD(void)
{
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize OpenGL loader GLAD" << std::endl;
        return false;
    }

    return true;
}

void init(void)
{
    shapeShader = new Shader("glslFiles/shader.vert", "glslFiles/shader.frag");
    forearmShader = new Shader("glslFiles/shader.vert", "glslFiles/shader.frag");

    upperArmShape = new Quadrilateral(glm::vec3(15.0f, 28.17f, 0.0f), glm::vec3(16.0f, 28.17f, 0.0f), glm::vec3(16.0f, 0.0f, 1.0f), glm::vec3(15.0f, 0.0f, 0.0f),
                                      glm::vec3(0.94f, 0.72f, 0.65f), glm::vec3(0.94f, 0.72f, 0.65f), glm::vec3(0.94f, 0.72f, 0.65f), glm::vec3(0.94f, 0.72f, 0.65f));
    
    forearmShape = new Quadrilateral(glm::vec3(15.0f, 0.5f, 0.0f), glm::vec3(41.89f, 0.5f, 0.0f), glm::vec3(41.89f, -0.5f, 0.0f), glm::vec3(15.0f, -0.5f, 0.0f),
                                     glm::vec3(0.94f, 0.72f, 0.65f), glm::vec3(0.94f, 0.72f, 0.65f), glm::vec3(0.94f, 0.72f, 0.65f), glm::vec3(0.94f, 0.72f, 0.65f));

    arm = new Arm();

}

void display(GLFWwindow * window)
{
    shapeShader->use();
    glm::mat4 model = glm::mat4(1.0f);
    shapeShader->setMat4("model", model);
    upperArmShape->render();


    //forearmShader->use();
    glm::mat4 forearmModel = glm::mat4(1.0f);
    forearmModel = glm::translate(forearmModel, glm::vec3(15.5f, 0.0f, 0.0f));
    forearmModel = glm::rotate(forearmModel, glm::radians((float)sliderTheta), glm::vec3(0.0, 0.0, 1.0));
    forearmModel = glm::translate(forearmModel, glm::vec3(-15.5f, 0.0f, 0.0f));
    
    //forearmShader->setMat4("model", forearmModel);
    shapeShader->setMat4("model", forearmModel);

    forearmShape->render();
}

static void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

void simulation(void)
{
    // Simulation calculations
    double tmpTheta = arm->getTheta(); // Starts with the initial theta's value

    //do
    //{
    //    physicsTime += physicsDeltaTime;

    //    for (unsigned int i = 0; i < physicsTick; i++)
    //    {
    //        tmpTheta = improvedEulerIntegration(calculationDeltaTime, tmpTheta);
    //    }

    //    physicsValues.time = physicsTime;
    //    physicsValues.angularAcc = angularAcc;
    //    physicsValues.angularVel = newAngularVel;
    //    physicsValues.theta = tmpTheta;

    //    cout << "Simulation, Physics Time: " << physicsTime << endl;

    //    // Add data to vector
    //    simulationData.push_back(physicsValues);

    //    // Write data to CSV File
    //    writeSimulationFileHeader();

    //} while (!stop);
    cout << "Finish simulation" << endl;
}