#pragma once

#include "UpperArmSection.h"
#include "ForearmSection.h"

class Arm
{
private:
	double _theta;


public:
	UpperArmSection* upperArm;
	ForearmSection* forearm;

	Arm(void);
	~Arm(void);

	/// <summary>
	/// Sets the value of the theta angle
	/// Theta: Angle respect to the horizontal between the elbow and the wrist
	/// </summary>
	void setTheta(double theta);

	/// <summary>
	/// Gets the value of the theta angle
	/// Theta: Angle respect to the horizontal between the elbow and the wrist
	/// </summary>
	/// <returns>Theta angle</returns>
	double getTheta(void);

	void setInitialPosition(void);


};