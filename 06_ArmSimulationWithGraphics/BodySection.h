#pragma once


class BodySection
{
private:
	double _mass;
	double _length;
	double _weight;

public:

	BodySection(void);
	BodySection(double mass, double length);
	~BodySection(void);

	void setMass(double mass);
	void setLength(double length);
	void setWeight(double gravityAcc);

	double getMass(void);
	double getLength(void);
	double getWeight(void);

};

