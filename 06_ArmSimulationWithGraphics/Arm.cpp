#include "Arm.h"

Arm::Arm(void)
{
	this->_theta = 0;
	this->setInitialPosition();
}

Arm::~Arm(void)
{}

void Arm::setInitialPosition(void)
{
	glm::vec3 shoulder = glm::vec3(30.0f, 70.0f, 0.0f);
	glm::vec3 elbow = glm::vec3(shoulder.x, shoulder.y - 28.17f, 0.0f);
	glm::vec3 wrist = glm::vec3(elbow.x + 26.89f, elbow.y, 0.0f);

	this->upperArm = new UpperArmSection(shoulder, elbow, 1.9783, 0.2817);
	this->forearm = new ForearmSection(elbow, wrist, 1.1826f, 0.2689f);
}

void Arm::setTheta(double theta)
{
	this->_theta = theta;
}

double Arm::getTheta(void)
{
	return this->_theta;
}