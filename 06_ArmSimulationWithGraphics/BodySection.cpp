#include "BodySection.h"

BodySection::BodySection(void)
{
	this->_mass = 0;
	this->_length = 0;
	this->_weight = 0;
}

BodySection::BodySection(double mass, double length)
{
	this->_mass = mass;
	this->_length = length;
	this->_weight = 0;
}

BodySection::~BodySection(void)
{}

void BodySection::setMass(double mass)
{
	this->_mass = mass;
}

void BodySection::setLength(double length)
{
	this->_length = length;
}

void BodySection::setWeight(double gravityAcc)
{
	this->_weight = this->_mass * gravityAcc;
}

double BodySection::getMass(void)
{
	return this->_mass;
}

double BodySection::getLength(void)
{
	return this->_length;
}

double BodySection::getWeight(void)
{
	return this->_weight;
}