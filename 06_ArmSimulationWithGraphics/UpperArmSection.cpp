#include "UpperArmSection.h"

UpperArmSection::UpperArmSection(glm::vec3 shoulderPos, glm::vec3 elbowPos, double mass, double length):BodySection(mass, length)
{
	this->_shoulderPos = shoulderPos;
	this->_elbowPos = elbowPos;
}

UpperArmSection::~UpperArmSection(void)
{}

glm::vec3 UpperArmSection::getShoulderPosition(void)
{
	return this->_shoulderPos;
}

glm::vec3 UpperArmSection::getElbowPosition(void)
{
	return this->_elbowPos;
}

void UpperArmSection::setShoulderPosition(glm::vec3 shoulderPos)
{
	this->_shoulderPos = shoulderPos;
}

void UpperArmSection::setElbowposition(glm::vec3 elbowPos)
{
	this->_elbowPos = elbowPos;
}