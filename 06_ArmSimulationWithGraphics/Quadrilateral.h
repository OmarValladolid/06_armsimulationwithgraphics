#pragma once

#include "Shader.h"
#include "glm/glm.hpp"

const unsigned int VerticesNumber = 12;
const unsigned int TrianglesNumber = 6;

class Quadrilateral
{
private:
	float			_vertices[VerticesNumber];
	float			_colors[VerticesNumber];
	unsigned int	_triangles[TrianglesNumber];
	unsigned int	_vbo[2], _vao, _ebo;

	void setVertices(float size);
	void setColors(void);
	void setTriangles(void);

public:
	Quadrilateral(float size);
	Quadrilateral(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3, glm::vec3 point4, glm::vec3 colorP1, glm::vec3 colorP2, glm::vec3 colorP3, glm::vec3 colorP4);
	~Quadrilateral(void);

	void setVertexAndColor(unsigned int vertexIndex, glm::vec3 vertex, glm::vec3 color);
	void setVertex(unsigned int vertexIndex, glm::vec3 vertex);
	void setColor(unsigned int vertexIndex, glm::vec3 color);
	void buildGeometry(Shader* shader);
	void render(void);
};

