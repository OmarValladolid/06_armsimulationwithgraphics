#pragma once

#include "BodySection.h"
#include "glm/glm.hpp"

class UpperArmSection : public BodySection
{
private:
	glm::vec3 _shoulderPos;
	glm::vec3 _elbowPos;

public:
	UpperArmSection(glm::vec3 shoulderPos, glm::vec3 elbowPos, double mass, double length);
	~UpperArmSection(void);

	glm::vec3 getShoulderPosition(void);
	glm::vec3 getElbowPosition(void);

	void setShoulderPosition(glm::vec3 shoulderPos);
	void setElbowposition(glm::vec3 elbowPos);
};

