#include "Quadrilateral.h"
#include "glm/glm.hpp"

Quadrilateral::Quadrilateral(float size)
{
    this->_vao = 0;
    this->_vbo[0] = 0;
    this->_vbo[1] = 0;
    this->_ebo = 0;

    if (size <= 0) size = 1;

    this->setVertices(size);
    this->setColors();
    this->setTriangles();
}

Quadrilateral::Quadrilateral(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3, glm::vec3 point4, glm::vec3 colorP1, glm::vec3 colorP2, glm::vec3 colorP3, glm::vec3 colorP4)
{
    this->_vao = 0;
    this->_vbo[0] = 0;
    this->_vbo[1] = 0;
    this->_ebo = 0;

    this->setVertexAndColor(0, point1, colorP1);
    this->setVertexAndColor(1, point2, colorP2);
    this->setVertexAndColor(2, point3, colorP3);
    this->setVertexAndColor(3, point4, colorP4);

    this->setTriangles();
}


Quadrilateral::~Quadrilateral(void)
{
    glDeleteVertexArrays(1, &this->_vao);
    glDeleteBuffers(1, &this->_vbo[0]);
    glDeleteBuffers(1, &this->_vbo[1]);
    glDeleteBuffers(1, &this->_ebo);
}

void Quadrilateral::setVertices(float size)
{
    // Vertex 1
    this->_vertices[0] = -size; this->_vertices[1] = size; this->_vertices[2] = 0.0f;
    // Vertex 2
    this->_vertices[3] = size; this->_vertices[4] = size; this->_vertices[5] = 0.0f;
    // Vertex 3
    this->_vertices[6] = size; this->_vertices[7] = -size; this->_vertices[8] = 0.0f;
    // Vertex 4
    this->_vertices[9] = -size; this->_vertices[10] = -size; this->_vertices[11] = 0.0f;
}

void Quadrilateral::setColors(void)
{
    for (unsigned int i = 0; i < VerticesNumber; i++)
    {
        this->_colors[i] = 1.0f;
    }
}

void Quadrilateral::setTriangles(void)
{
    // Triangles
    this->_triangles[0] = 0; this->_triangles[1] = 1; this->_triangles[2] = 2; // First triangle
    this->_triangles[3] = 2; this->_triangles[4] = 3; this->_triangles[5] = 0; // Second triangle
}

void Quadrilateral::setVertexAndColor(unsigned int vertexIndex, glm::vec3 vertex, glm::vec3 color)
{
    unsigned int tmpIndex = vertexIndex * 3;
    this->_vertices[tmpIndex] = vertex.x; this->_vertices[tmpIndex + 1] = vertex.y; this->_vertices[tmpIndex + 2] = vertex.z;
    this->_colors[tmpIndex] = color.r; this->_colors[tmpIndex + 1] = color.g; this->_colors[tmpIndex + 2] = color.b;
}

void Quadrilateral::setVertex(unsigned int vertexIndex, glm::vec3 vertex)
{
    unsigned int tmpIndex = vertexIndex * 3;
    this->_vertices[tmpIndex] = vertex.x; this->_vertices[tmpIndex + 1] = vertex.y; this->_vertices[tmpIndex + 2] = vertex.z;
}

void Quadrilateral::setColor(unsigned int vertexIndex, glm::vec3 color)
{
    unsigned int tmpIndex = vertexIndex * 3;
    this->_colors[tmpIndex] = color.r; this->_colors[tmpIndex + 1] = color.g; this->_colors[tmpIndex + 2] = color.b;
}

void Quadrilateral::buildGeometry(Shader* shader)
{
    glGenVertexArrays(1, &this->_vao);
    glGenBuffers(2, this->_vbo);
    glGenBuffers(1, &this->_ebo);
    // bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
    glBindVertexArray(this->_vao);

    // Position attribute
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(this->_vertices), this->_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(shader->getAttributeLocation("inPos"), 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    // Color attribute
    glBindBuffer(GL_ARRAY_BUFFER, this->_vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(this->_colors), this->_colors, GL_STATIC_DRAW);
    glVertexAttribPointer(shader->getAttributeLocation("inColor"), 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(1);

    // Element buffer object
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(this->_triangles), this->_triangles, GL_STATIC_DRAW);

    glBindVertexArray(0);
}

void Quadrilateral::render(void)
{
    glBindVertexArray(this->_vao);
    glDrawElements(GL_TRIANGLES, TrianglesNumber, GL_UNSIGNED_INT, 0);
}