#pragma once

class PhysicsSimulation
{
private:

	unsigned int _physicsTime;
	unsigned int _physicsTick;
	double _physicsDeltaTime;
	double _calculationDeltaTime;

public:
	PhysicsSimulation(void);
	~PhysicsSimulation(void);

	void run(void);

	double improvedEulerIntegration(double dt, double currentTheta);
};